# Example RECAST Analysis

This is a starting point repo for a RECAST project.

To get started do the following:

* go to gitlab CI/CD settings to 
* set these secret envirnoment variables


```
RECAST_AUTH_USERNAME <-- your CERN username
RECAST_AUTH_PASSWORD <-- your CERN password
RECAST_REGISTRY_USERNAME <-- your CERN username
RECAST_REGISTRY_PASSWORD <-- your personal access token (or CERN password)
```
